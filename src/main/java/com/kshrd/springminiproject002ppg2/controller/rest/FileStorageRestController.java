package com.kshrd.springminiproject002ppg2.controller.rest;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/files")
public class FileStorageRestController {

    @GetMapping("/{filename}")
    public ResponseEntity<?> getFile(@PathVariable String filename){
        Map<String, Object> response = new HashMap<>();

        response.put("status", "OK");
        response.put("message", "file get success");
        response.put("success", true);
        response.put("payload", "put data here");

        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteAllFile(){
        Map<String, Object> response = new HashMap<>();

        response.put("status", "OK");
        response.put("message", "file delete success");
        response.put("success", true);
        response.put("payload", "put data here");

        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/upload")
    public ResponseEntity<?> uploadFile(){
        Map<String, Object> response = new HashMap<>();

        response.put("status", "OK");
        response.put("message", "file upload success");
        response.put("success", true);
        response.put("payload", "put data here");

        return ResponseEntity.ok().body(response);
    }

}
