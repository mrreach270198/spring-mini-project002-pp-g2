package com.kshrd.springminiproject002ppg2.controller.rest;


import com.kshrd.springminiproject002ppg2.model.User;
import com.kshrd.springminiproject002ppg2.payload.dto.UserDetailLogin;
import com.kshrd.springminiproject002ppg2.payload.request.LoginRequest;
import com.kshrd.springminiproject002ppg2.payload.request.UserRequest;
import com.kshrd.springminiproject002ppg2.security.jwt.JWTUtil;
import com.kshrd.springminiproject002ppg2.service.MyUserDetailService;
import com.kshrd.springminiproject002ppg2.service.UserService;
import com.kshrd.springminiproject002ppg2.service.imp.UserServiceImp;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/api/auth")
public class AuthRestController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailService userDetailService;

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Operation(summary = "Login to get token")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "New Employee added", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(schema = @Schema(hidden = true))}),
            @ApiResponse(responseCode = "401", description = "Un-Authorized user", content = {@Content(schema = @Schema(hidden = true))}),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = {@Content(schema = @Schema(hidden = true))})
    })

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest) throws Exception {
        Map<String, Object> response = new HashMap<>();
        if (userService.checkAccIsClose(loginRequest.getUsername())){
            response.put("status", "failed");
            response.put("message", "Your account is close");
            response.put("success", false);
        }else {

            User user = userService.loadUserByUsername(loginRequest.getUsername());

            System.out.println("Role : " + userService.getRolesByUserId(user.getId()));

            UserDetailLogin userDetailLogin = new UserDetailLogin();

            userDetailLogin.setId(user.getId());
            userDetailLogin.setUsername(user.getUsername());
            userDetailLogin.setRoles(userService.getRolesByUserId(user.getId()));

            try {
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())
                );
            } catch (BadCredentialsException e) {
                throw new Exception("Incorrect username password", e);
            }

            final UserDetails userDetails = userDetailService
                    .loadUserByUsername(loginRequest.getUsername());
            final String jwt = jwtUtil.generateToken(userDetails);

            userDetailLogin.setToken(jwt);

            response.put("status", "OK");
            response.put("message", "login success");
            response.put("success", true);
            response.put("payload", userDetailLogin);
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody UserRequest userRequest){

        User user = new User();

        String userId = UUID.randomUUID().toString();
        user.setId(userId);
        user.setUsername(userRequest.getUsername());
        user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        user.setFullName(userRequest.getFullName());

        user.setProfile("default.png");
        user.setIsAccClose(false);

        userService.insertUser(user);

        int roleId = userService.getRoleId("ROLE_USER");

        userService.insertUserRole(userId, roleId);

        Map<String, Object> response = new HashMap<>();

        response.put("status", "OK");
        response.put("message", "register success");
        response.put("success", true);
        response.put("payload", user);

        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/refresh-token")
    public ResponseEntity<?> refreshToken(){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            String username = ((UserDetails)principal).getUsername();
            System.out.println("Username1 : " + username);
        } else {
            String username = principal.toString();
        }
        Map<String, Object> response = new HashMap<>();

        response.put("status", "OK");
        response.put("message", "Token refresh success");
        response.put("success", true);
        response.put("payload", "put data here");

        return ResponseEntity.ok().body(response);
    }
}
