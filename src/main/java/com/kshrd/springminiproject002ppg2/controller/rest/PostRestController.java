
package com.kshrd.springminiproject002ppg2.controller.rest;

import com.kshrd.springminiproject002ppg2.model.Post;
import com.kshrd.springminiproject002ppg2.model.User;
import com.kshrd.springminiproject002ppg2.payload.dto.PostDto;
import com.kshrd.springminiproject002ppg2.payload.map.PostMapper;
import com.kshrd.springminiproject002ppg2.payload.request.PostRequest;
import com.kshrd.springminiproject002ppg2.repository.CommentRepository;
import com.kshrd.springminiproject002ppg2.repository.PostRepository;
import com.kshrd.springminiproject002ppg2.service.CommentService;
import com.kshrd.springminiproject002ppg2.service.imp.PostServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/posts")
public class PostRestController {

    @Autowired
    private PostServiceImp postService;

    @GetMapping
    public ResponseEntity<?> findAllPosts(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;

        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
            System.out.println("Username1 : " + username);
        } else {
            username = principal.toString();
        }
        System.out.println("Username"+username);
        Map<String, Object> response = new HashMap<>();
        List<Post> posts = postService.getAllPost();
        String userid = postService.getUserId(username);
        System.out.println("Userid : "+userid);

        for(int i = 0; i<posts.size(); i++){
            if(userid.equals(posts.get(i).getUserid())){
                posts.get(i).setOwner(true);
            }
        }
        response.put("status", "OK");
        response.put("message", "get all posts success");
        response.put("success", true);
        response.put("payload", posts);

        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> deletePostById(@PathVariable int id) throws Exception {
        Map<String, Object> response = new HashMap<>();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        String userid = postService.getUserId(username);
        if(userid.equals(postService.getUserByPostId(id))){
            Post post = postService.deletePostByID(id);
            response.put("status", "OK");
            response.put("message", "delete post is success");
            response.put("success", true);
            response.put("payload", post);
        }else{
            response.put("status", "Exception");
            response.put("message", "you are not the owner");
            response.put("success", false);
        }

        return ResponseEntity.ok().body(response);
    }

    @PutMapping("/{id}/update")
    public ResponseEntity<?> updatePost(@PathVariable int id, @RequestBody PostRequest postRequest){
        Map<String, Object> response = new HashMap<>();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;


        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        String userid = postService.getUserId(username);
        if(userid.equals(postService.getUserByPostId(id))){
            postService.updateComment(id, postRequest);
            response.put("status", "OK");
            response.put("message", "update post is success");
            response.put("success", true);
            Post post = postService.findPostbyId(id);
            post.setOwner(true);
            response.put("payload", post);
        }else{
            response.put("status", "Exception");
            response.put("message", "you are not the owner");
            response.put("success", false);
        }


        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/view/{id}")
    public ResponseEntity<?> findPostById(@PathVariable int id){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;

        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
            System.out.println("Username1 : " + username);
        } else {
            username = principal.toString();
        }
        Map<String, Object> response = new HashMap<>();
        Post post = postService.findPostbyId(id);
        String userid = postService.getUserId(username);
            if(userid.equals(post.getUserid())){
                post.setOwner(true);
            }
        response.put("status", "OK");
        response.put("message", "post found");
        response.put("success", true);
        response.put("payload", "put data here");

        return ResponseEntity.ok().body(post);
    }

    @PostMapping(value="/create")
    public ResponseEntity<?> createPost(@RequestBody PostRequest postRequest){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;
        Map<String, Object> response = new HashMap<>();
        Post post = new Post();
        post.setCaption(postRequest.getCaption());
        post.setImg(postRequest.getImg());
        post.setNumberoflike(postRequest.getNumberoflike());
        post.setOwner(true);

        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        String id = postService.getUserId(username);
        System.out.println("ID "+id);
//
        post.setUserid(id);
        postService.insertPost(post);
        response.put("status", "OK");
        response.put("message", "post create success");
        response.put("success", true);
        response.put("payload", post);
//
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/filter")
    @ResponseBody
    public ResponseEntity<?> findPostByFilter(@RequestParam(required = false) String userId,
                                              @RequestParam(required = false) String userName,
                                              @RequestParam(required = false) String caption ){
        Map<String, Object> response = new HashMap<>();
        List<Post>filtering = postService.filteringPost(userId,userName,caption);
        response.put("status", "OK");
        response.put("message", "post filter success");
        response.put("success", true);
        response.put("payload", "put data here");

        return ResponseEntity.ok().body(filtering);
    }

    @PatchMapping("/react")
    public ResponseEntity<?> likePost(){
        Map<String, Object> response = new HashMap<>();

        response.put("status", "OK");
        response.put("message", "post like success");
        response.put("success", true);
        response.put("payload", "put data here");

        return ResponseEntity.ok().body(response);
    }
}