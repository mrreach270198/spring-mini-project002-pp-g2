package com.kshrd.springminiproject002ppg2.controller.rest;


import com.kshrd.springminiproject002ppg2.payload.request.AccountRequest;
import com.kshrd.springminiproject002ppg2.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/account")
public class AccountRestController {

    @Autowired
    private AccountService accountService;

    @PatchMapping("/{id}/close")
    public ResponseEntity<?> closeAccount(@PathVariable String id){


        Map<String, Object> response = new HashMap<>();

        if (accountService.closeAccountById(id, true)){
            response.put("status", "OK");
            response.put("message", "Account close success");
            response.put("success", true);
            response.put("payload", id);
        }else {
            response.put("status", "failed");
            response.put("message", "Account close not success");
        }


        return ResponseEntity.ok().body(response);
    }

    @PatchMapping("/{id}/update-profile")
    public ResponseEntity<?> updateProfile(@PathVariable String id, @RequestBody AccountRequest accountRequest){



        Map<String, Object> response = new HashMap<>();

        if (accountService.updateAccount(id, accountRequest)){
            response.put("status", "OK");
            response.put("message", "profile update success");
            response.put("success", true);
            response.put("payload", accountRequest);
        }else {
            response.put("status", "failed");
            response.put("message", "profile update not success");
        }


        return ResponseEntity.ok().body(response);
    }
}
