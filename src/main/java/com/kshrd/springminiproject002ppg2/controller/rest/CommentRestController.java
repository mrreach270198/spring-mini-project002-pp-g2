package com.kshrd.springminiproject002ppg2.controller.rest;


import com.kshrd.springminiproject002ppg2.model.Comment;
import com.kshrd.springminiproject002ppg2.model.CommentReply;
import com.kshrd.springminiproject002ppg2.payload.dto.CommentDtoByPost;
import com.kshrd.springminiproject002ppg2.payload.dto.CommentReplyDto;
import com.kshrd.springminiproject002ppg2.payload.dto.PostCommentDto;
import com.kshrd.springminiproject002ppg2.payload.dto.ReplyDetail;
import com.kshrd.springminiproject002ppg2.payload.map.CommentMap;
import com.kshrd.springminiproject002ppg2.payload.request.CommentReplyRequest;
import com.kshrd.springminiproject002ppg2.payload.request.CommentRequest;
import com.kshrd.springminiproject002ppg2.repository.CommentRepository;
import com.kshrd.springminiproject002ppg2.service.imp.CommentServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
public class CommentRestController {

    @Autowired
    private CommentServiceImp commentServiceImp;

    @Autowired
    private CommentMap commentMap;




    @PostMapping("/api/comments")//====> done
    public ResponseEntity<?> insertComment(@RequestBody CommentRequest commentRequest){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;

        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
            System.out.println("Username1 : " + username);
        } else {
            username = principal.toString();
        }

        String id = commentServiceImp.getUserId(username);

        Map<String, Object> response = new HashMap<>();

//        Comment comment = new Comment();
//        comment.setPost_id(commentRequest.getPost_id());
//        comment.setComment_content(commentRequest.getComment_content());
//        comment.setUser_id(commentRequest.getUser_id());

        //commentRequest.setUser_id(id);
        commentServiceImp.insertComment(commentRequest,id);

//      CommentDto commentDto = commentMap.commentTocommentDto(comment);

        response.put("status", "OK");
        response.put("message", "comment insert success");
        response.put("success", true);
        response.put("payload", commentRequest);

        return ResponseEntity.ok().body(response);
    }





    @GetMapping("/api/comments/{id}/replies") //====> done
    public ResponseEntity<?> findRepliesByCommentId(@PathVariable int id){
        Map<String, Object> response = new HashMap<>();

        Comment comment = commentServiceImp.fetchAllCommentReplyByCommentId(id);

        response.put("status", "OK");
        response.put("message", "replies found");
        response.put("success", true);
        response.put("payload", comment);

        return ResponseEntity.ok().body(response);
    }






    @GetMapping("/api/posts/{id}/comments")
    public ResponseEntity<?> findCommentsByPostId(@RequestParam int id){
        Map<String, Object> response = new HashMap<>();


        PostCommentDto post = commentServiceImp.getPost(id);
        //List<CommentDtoByPost> lists = commentRepository.getCommentByPostId(id);


        response.put("status", "OK");
        response.put("message", "comment found");
        response.put("success", true);
        response.put("payload", post);

        return ResponseEntity.ok().body(response);
    }





    @PostMapping("/api/replies")//====> done
    public ResponseEntity<?> insertReplay(@RequestBody CommentReplyRequest commentReplyRequest){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username;

        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
            System.out.println("Username1 : " + username);
        } else {
            username = principal.toString();
        }

        String id = commentServiceImp.getUserId(username);


        Map<String, Object> response = new HashMap<>();

        int postId = commentServiceImp.getPostIdByCommentId(commentReplyRequest.getComment_id());

        commentServiceImp.insertCommentReply(commentReplyRequest,id,postId);

        ReplyDetail replyDetail = new ReplyDetail();
        replyDetail.setUserId(id);
        replyDetail.setCommentId(commentReplyRequest.getComment_id());
        replyDetail.setCommentReplyContent(commentReplyRequest.getComment_reply_content());

//        CommentReply commentReply = commentMap.comReplyResquestTocomReply(commentReplyRequest);

        //commentReply.setUser_id(id);

        //System.out.println(commentReply);

        //commentReply.setPost_id(commentServiceImp.getPostIdByCommentId(commentReplyRequest.getComment_id()));
//
//        boolean isInsertedCommentReply =  commentServiceImp.insertCommentReply(commentReply,id);
//
//        CommentReplyDto commentReplyDto = commentMap.comReplyTocommentReplyDto(commentReply);
//
//        commentReplyDto.setPost_id(commentReply.getPost_id());

            response.put("status", "OK");
            response.put("message", "reply insert success");
            response.put("success", true);
            response.put("payload", replyDetail);


        return ResponseEntity.ok().body(response);
    }




}
