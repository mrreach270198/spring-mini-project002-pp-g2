package com.kshrd.springminiproject002ppg2.service;

import com.kshrd.springminiproject002ppg2.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    public Boolean insertUser(User user);
    public String getUserId(String username);
    public int getRoleId(String role);
    public Boolean insertUserRole(String userId, int roleId);
    public User checkUserLogin(String username, String password);
    User loadUserByUsername(String username);
    List<String> getRolesByUserId(String userId);
    Boolean checkAccIsClose(String username);

}
