package com.kshrd.springminiproject002ppg2.service;

import com.kshrd.springminiproject002ppg2.payload.request.AccountRequest;

public interface AccountService {

    Boolean updateAccount(String userId, AccountRequest accountRequest);
    Boolean closeAccountById(String userId, Boolean status);
}
