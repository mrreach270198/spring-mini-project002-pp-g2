package com.kshrd.springminiproject002ppg2.service.imp;

import com.kshrd.springminiproject002ppg2.model.User;
import com.kshrd.springminiproject002ppg2.repository.UserRepository;
import com.kshrd.springminiproject002ppg2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Boolean insertUser(User user) {
        return userRepository.insertUser(user);
    }

    @Override
    public String getUserId(String username) {
        return userRepository.getUserId(username);
    }

    @Override
    public int getRoleId(String role) {
        return userRepository.getRoleId(role);
    }

    @Override
    public Boolean insertUserRole(String userId, int roleId) {
        return userRepository.insertUserRole(userId, roleId);
    }

    @Override
    public User checkUserLogin(String username, String password) {
        return userRepository.checkUserLogin(username, password);
    }

    @Override
    public User loadUserByUsername(String username) {
        return userRepository.loadUserByUsername(username);
    }

    @Override
    public List<String> getRolesByUserId(String userId) {
        return userRepository.getRolesByUserId(userId);
    }

    @Override
    public Boolean checkAccIsClose(String username) {
        return userRepository.checkAccIsClose(username);
    }
}
