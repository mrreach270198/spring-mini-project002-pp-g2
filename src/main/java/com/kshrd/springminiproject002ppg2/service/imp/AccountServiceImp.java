package com.kshrd.springminiproject002ppg2.service.imp;

import com.kshrd.springminiproject002ppg2.payload.request.AccountRequest;
import com.kshrd.springminiproject002ppg2.repository.AccountRepository;
import com.kshrd.springminiproject002ppg2.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImp implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Boolean updateAccount(String userId, AccountRequest accountRequest) {
        return accountRepository.updateAccount(userId, accountRequest);
    }

    @Override
    public Boolean closeAccountById(String userId, Boolean status) {
        return accountRepository.closeAccountById(userId, status);
    }
}
