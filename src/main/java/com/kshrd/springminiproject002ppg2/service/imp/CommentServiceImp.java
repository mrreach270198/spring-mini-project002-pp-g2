package com.kshrd.springminiproject002ppg2.service.imp;


import com.kshrd.springminiproject002ppg2.model.Comment;
import com.kshrd.springminiproject002ppg2.model.CommentReply;
import com.kshrd.springminiproject002ppg2.payload.dto.PostCommentDto;
import com.kshrd.springminiproject002ppg2.payload.request.CommentReplyRequest;
import com.kshrd.springminiproject002ppg2.payload.request.CommentRequest;
import com.kshrd.springminiproject002ppg2.repository.CommentRepository;
import com.kshrd.springminiproject002ppg2.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImp implements CommentService {

    @Autowired
    private CommentRepository commentRepository;


    @Override
    public String getUserId(String username) {
        return commentRepository.getUserId(username);
    }


    @Override
    public boolean insertComment(CommentRequest commentRequest,String id) {

        boolean isInserted = commentRepository.insertComment(commentRequest,id);

        return isInserted;

    }



    @Override
    public boolean insertCommentReply(CommentReplyRequest commentReplyRequest,String id, int postId) {

        boolean isInsertReply = commentRepository.insertCommentReply(commentReplyRequest,id,postId);

        return isInsertReply;

    }



    @Override
    public int getPostIdByCommentId(int commentId) {
        int id = commentRepository.getPostIdByCommentId(commentId);
        return id;
    }



    @Override
    public Comment fetchAllCommentReplyByCommentId(int comment_id) {

        Comment comment = commentRepository.fetchAllCommentReplyByCommentId(comment_id);

        return comment;
    }

    @Override
    public PostCommentDto getPost(int postId) {
        return commentRepository.getPost(postId);
    }


}
