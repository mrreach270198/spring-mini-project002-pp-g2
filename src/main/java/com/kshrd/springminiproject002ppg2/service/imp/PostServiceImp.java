package com.kshrd.springminiproject002ppg2.service.imp;

import com.kshrd.springminiproject002ppg2.model.Post;
import com.kshrd.springminiproject002ppg2.payload.request.PostRequest;
import com.kshrd.springminiproject002ppg2.repository.PostRepository;
import com.kshrd.springminiproject002ppg2.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImp implements PostService {

    private PostRepository postRepository;

    @Autowired
    public PostServiceImp(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public List<Post> getAllPost() {
        return postRepository.getAllPost();
    }

    @Override
    public Post findPostbyId(int id) {
        return postRepository.findPostbyId(id);
    }

    @Override
    public Post deletePostByID(int id) {
        return postRepository.deletePostByID(id);
    }

    @Override
    public Boolean insertPost(Post post) {
        return postRepository.insertPost(post);
    }

    @Override
    public String getUserId(String username) {
        return postRepository.getUserId(username);
    }

    @Override
    public Boolean updateComment(int id, PostRequest request) {
        return postRepository.updateComment(id ,request);
    }

    @Override
    public String getUserByPostId(int postid) {
        return postRepository.getUserByPostId(postid);
    }

    @Override
    public List<Post> filteringPost(String userid, String userName, String caption) {
        return postRepository.filteringPost(userid,userName, caption);
    }
}
