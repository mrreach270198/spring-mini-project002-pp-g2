package com.kshrd.springminiproject002ppg2.service;

import com.kshrd.springminiproject002ppg2.model.User;
import com.kshrd.springminiproject002ppg2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.loadUserByUsername(username);

//        if(user != null) {
//            UserDetailImp userDetailImp = new UserDetailImp();
//            userDetailImp.setUsername(user.getUsername());
//            userDetailImp.setPassword(user.getPassword());
//            return userDetailImp;
//        }else {
//            throw new UsernameNotFoundException("User not found");
//        }
        if(user==null)throw new UsernameNotFoundException("User not found");
        return new CustomUserDetails(user);
    }
}
