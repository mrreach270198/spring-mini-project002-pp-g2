package com.kshrd.springminiproject002ppg2.service;

import com.kshrd.springminiproject002ppg2.model.Post;
import com.kshrd.springminiproject002ppg2.payload.request.PostRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostService {


    List<Post> getAllPost();


    Post findPostbyId(int id);


    Post deletePostByID(int id);

    List<Post> filteringPost(String userid, String userName, String caption);

    Boolean insertPost(Post post);
    String getUserId(String username);

    Boolean updateComment (int id , PostRequest request);

    String getUserByPostId(int postid);
}
