package com.kshrd.springminiproject002ppg2.service;


import com.kshrd.springminiproject002ppg2.model.Comment;
import com.kshrd.springminiproject002ppg2.model.CommentReply;
import com.kshrd.springminiproject002ppg2.payload.dto.PostCommentDto;
import com.kshrd.springminiproject002ppg2.payload.request.CommentReplyRequest;
import com.kshrd.springminiproject002ppg2.payload.request.CommentRequest;
import org.apache.ibatis.annotations.Param;

public interface CommentService {

    String getUserId(String username);


    boolean insertComment(CommentRequest commentRequest,String id);

    boolean insertCommentReply(CommentReplyRequest commentReplyRequest,String id, int postId);

    int getPostIdByCommentId(@Param("id") int commentId);

    Comment fetchAllCommentReplyByCommentId(int comment_id);

    PostCommentDto getPost(int postId);

}
