package com.kshrd.springminiproject002ppg2.payload.map;


import com.kshrd.springminiproject002ppg2.model.Comment;
import com.kshrd.springminiproject002ppg2.model.CommentReply;
import com.kshrd.springminiproject002ppg2.payload.dto.CommentDto;
import com.kshrd.springminiproject002ppg2.payload.dto.CommentDtoByPost;
import com.kshrd.springminiproject002ppg2.payload.dto.CommentReplyDto;
import com.kshrd.springminiproject002ppg2.payload.request.CommentReplyRequest;
import com.kshrd.springminiproject002ppg2.payload.request.CommentRequest;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface CommentMap {

    //ccomment--->>
    Comment comRequestTocom(CommentRequest commentRequest);

    CommentDto commentTocommentDto(Comment comment);



    //commentReply--->>
    CommentReply comReplyResquestTocomReply(CommentReplyRequest commentReplyRequest);

    CommentReplyDto comReplyTocommentReplyDto(CommentReply commentReply);


}
