package com.kshrd.springminiproject002ppg2.payload.map;

import com.kshrd.springminiproject002ppg2.model.Post;
import com.kshrd.springminiproject002ppg2.payload.dto.PostDto;
import com.kshrd.springminiproject002ppg2.payload.request.PostRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface PostMapper {
//    @Mapping(source = "userid", target = "userid")
//    PostDto postToPostDto(Post post);
//
//    @Mapping(source = "caption", target = "caption")
//    Post postRequestToPost(PostRequest postRequest);
}
