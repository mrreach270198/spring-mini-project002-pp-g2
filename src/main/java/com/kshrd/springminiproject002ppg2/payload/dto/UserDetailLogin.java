package com.kshrd.springminiproject002ppg2.payload.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailLogin {

    private String id;
    private String username;
    private String token;
    private List<String> roles;

}
