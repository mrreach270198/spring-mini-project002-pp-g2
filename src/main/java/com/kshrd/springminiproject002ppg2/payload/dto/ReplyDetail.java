package com.kshrd.springminiproject002ppg2.payload.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ReplyDetail {

    private String userId;
    private int commentId;
    private String commentReplyContent;

}
