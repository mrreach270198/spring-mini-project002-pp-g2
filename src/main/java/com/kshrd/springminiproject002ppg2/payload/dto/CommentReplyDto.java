package com.kshrd.springminiproject002ppg2.payload.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CommentReplyDto {

    private int post_id;
    private int comment_id;
    private String comment_reply_content;

}
