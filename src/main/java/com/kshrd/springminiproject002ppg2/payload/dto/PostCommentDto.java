package com.kshrd.springminiproject002ppg2.payload.dto;


import com.kshrd.springminiproject002ppg2.model.Comment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class PostCommentDto {


    private int postId;
    private String userId;
    private String image;
    private int numberoflike;
    private String caption;

    private List<CommentDtoByPost> comments;


}
