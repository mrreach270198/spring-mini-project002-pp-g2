package com.kshrd.springminiproject002ppg2.payload.dto;


import com.kshrd.springminiproject002ppg2.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CommentDto {
    private int post_id;
    private String comment_content;
    private User user;
}
