package com.kshrd.springminiproject002ppg2.payload.dto;


import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PostDto {
    private int id;
    private String userid;
    private String userName;
    private String img;
    private boolean owner;
    private String caption;
    private int numberOfLike;

}
