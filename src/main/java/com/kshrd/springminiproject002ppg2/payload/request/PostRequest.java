package com.kshrd.springminiproject002ppg2.payload.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PostRequest {

    private String caption;
    private int numberoflike;
    private String img;
}
