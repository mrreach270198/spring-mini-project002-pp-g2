package com.kshrd.springminiproject002ppg2.payload.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CommentReplyRequest {

    private int comment_id;
    private String comment_reply_content;
    //private String user_id;

}
