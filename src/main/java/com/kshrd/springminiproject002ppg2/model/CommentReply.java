package com.kshrd.springminiproject002ppg2.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CommentReply {

    private int comment_reply_id;
    private int post_id;
    private int comment_id;
    private String user_id;
    private User user;
    private String comment_reply_content;

}
