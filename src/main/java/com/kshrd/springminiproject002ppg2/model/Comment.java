package com.kshrd.springminiproject002ppg2.model;


import com.kshrd.springminiproject002ppg2.payload.dto.CommentReplyDtoList;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Comment {

    private int comment_id;
    private int post_id;
    private String comment_content;
    private  String user_id;
    private User user;

    private List<CommentReplyDtoList> commentReplyList;

}
