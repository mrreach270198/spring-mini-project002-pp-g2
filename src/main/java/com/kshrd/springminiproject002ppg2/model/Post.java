package com.kshrd.springminiproject002ppg2.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Post {
    private int id;
    private String userid;
    private String username;
    private String img;
    private boolean owner;
    private String caption;
    private int numberoflike;
}
