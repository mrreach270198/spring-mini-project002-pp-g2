package com.kshrd.springminiproject002ppg2.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private String id;
    private String username;
    private String password;
    private String fullName;
    private String profile;
    private Boolean isAccClose;
}
