package com.kshrd.springminiproject002ppg2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMiniproject002PpG2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringMiniproject002PpG2Application.class, args);
    }

}
