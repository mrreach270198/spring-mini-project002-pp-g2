package com.kshrd.springminiproject002ppg2.repository;


import com.kshrd.springminiproject002ppg2.model.Comment;
import com.kshrd.springminiproject002ppg2.model.CommentReply;
import com.kshrd.springminiproject002ppg2.model.User;
import com.kshrd.springminiproject002ppg2.payload.dto.CommentDtoByPost;
import com.kshrd.springminiproject002ppg2.payload.dto.CommentReplyDtoList;
import com.kshrd.springminiproject002ppg2.payload.dto.PostCommentDto;
import com.kshrd.springminiproject002ppg2.payload.request.CommentReplyRequest;
import com.kshrd.springminiproject002ppg2.payload.request.CommentRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface CommentRepository {


    @Select("select id from tb_users where username = #{username}")
    String getUserId(String username);


    @Insert("insert into table_comment(comment_content , post_id , user_id) " +
            "VALUES (#{comment.comment_content},#{comment.post_id},#{id})")
    boolean insertComment(@Param("comment") CommentRequest commentRequest,String id);



    @Select("select * from tb_posts where id=#{postId}")
    @Result(property = "postId",column = "id")
    @Result(property = "image",column = "img")
    @Result(property = "userId",column = "userid")
    @Result(property = "comments",column = "id",many=@Many(select = "getCommentByPostId"))
    PostCommentDto getPost(@Param("postId") int postId);




    @Select("select * from table_comment where post_id = #{postId}")
    @Result(property = "user",column = "user_id",many = @Many(select = "fetchUserInfo"))
    List<CommentDtoByPost> getCommentByPostId(int postId);




    @Insert("insert into table_comment_reply(comment_reply_content, comment_id,post_id,user_id) " +
            "VALUES (#{commentReplyRequest.comment_reply_content},#{commentReplyRequest.comment_id},#{postId},#{id})")
    boolean insertCommentReply(CommentReplyRequest commentReplyRequest,String id, int postId );

    @Select("select post_id from table_comment where comment_id = #{id}")
    int getPostIdByCommentId(@Param("id") int commentId);




    @Select("select * from table_comment where comment_id = #{id}")
    @Result(property = "commentReplyList",column = "comment_id",many = @Many(select = "fetchAllCommentReply"))
    @Result(property = "comment_id",column = "comment_id")
    @Result(property = "user",column = "user_id",many = @Many(select = "fetchUserInfo"))
    Comment fetchAllCommentReplyByCommentId(@Param("id") int comment_id);

    @Select("select * from table_comment_reply where comment_id= #{id}")
    @Result(property = "user",column = "user_id",many =@Many(select = "fetchUserInfo"))
    List<CommentReplyDtoList> fetchAllCommentReply(@Param("id") int comment_id);

    @Select("select * from tb_users where id = #{id}")
    @Result(property = "fullName",column = "full_name")
    @Result(property = "isAccClose",column = "is_acc_close")
    User fetchUserInfo(@Param("id") String user_id);


}
