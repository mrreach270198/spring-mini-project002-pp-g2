package com.kshrd.springminiproject002ppg2.repository;


import com.kshrd.springminiproject002ppg2.payload.request.AccountRequest;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository {

    @Update("update tb_users set username=#{accountRequest.username}, full_name=#{accountRequest.fullName}, profile=#{accountRequest.imageUrl} where id=#{userId}")
    Boolean updateAccount(String userId, AccountRequest accountRequest);

    @Update("update tb_users set is_acc_close=#{status} where id=#{userId}")
    Boolean closeAccountById(String userId, Boolean status);
}
