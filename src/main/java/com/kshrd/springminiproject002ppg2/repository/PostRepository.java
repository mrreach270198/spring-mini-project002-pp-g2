package com.kshrd.springminiproject002ppg2.repository;

import com.kshrd.springminiproject002ppg2.model.Post;
import com.kshrd.springminiproject002ppg2.payload.request.PostRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository {

    @Select("select tb_posts.id, tb_posts.userid, tb_posts.numberoflike, tb_posts.caption, tb_posts.img, tu.username from tb_posts inner join tb_users tu on tu.id = tb_posts.userid")
    List<Post> getAllPost();

    @Select("select tb_posts.id, tb_posts.userid, tb_posts.numberoflike, tb_posts.caption, tb_posts.img, tu.username from tb_posts inner join tb_users tu on tu.id = tb_posts.userid where tb_posts.id = #{id}")
    @Result(property = "id", column = "id")
    Post findPostbyId(int id);

    @Select("delete from tb_posts where id = #{id}")
     Post deletePostByID(int id);

    @Select("select tb_posts.id, tb_posts.userid, tb_posts.numberoflike, tb_posts.caption, tb_posts.img, tu.username from tb_posts inner join tb_users tu on tu.id = tb_posts.userid where tb_posts.userid = #{userid} or tu.username = #{username} or tb_posts.caption = #{caption}")
    @Result(property = "userid", column = "userid")
    @Result(property = "username", column = "username")
    @Result(property = "caption", column = "caption")
    List<Post> filteringPost(String userid, String username, String caption);

    @Insert("insert into tb_posts (userid, numberoflike, caption , img) values (#{post.userid},#{post.numberoflike}, #{post.caption}, #{post.img}) ")
    Boolean insertPost(@Param("post") Post post);

    @Select("select id from tb_users where username = #{username}")
    public String getUserId(String username);

    @Select("select userid from tb_posts where id = #{postid}")
    String getUserByPostId(int postid);

    @Update("update tb_posts set caption = #{request.caption}, numberoflike = #{request.numberoflike}, img = #{request.img} where id = #{id}")
    Boolean updateComment (int id, PostRequest request);

}

