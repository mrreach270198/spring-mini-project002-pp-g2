package com.kshrd.springminiproject002ppg2.repository;

import com.kshrd.springminiproject002ppg2.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    @Select("select *from tb_users where username = #{username}")
    User loadUserByUsername(String username);

    @Select("select r.role from tb_users_role as ur inner join tb_roles as r on ur.role_id = r.id where ur.user_id = #{userId}")
    List<String> getRolesByUserId(String userId);

    @Insert("insert into tb_users (id, username, password, full_name, profile, is_acc_close) values(#{user.id}, #{user.username}, #{user.password}, #{user.fullName},#{user.profile}, #{user.isAccClose})")
    public Boolean insertUser(@Param("user") User user);

    @Select("select id from tb_users where username=#{username}")
    public String getUserId(String username);

    @Select("select id from tb_roles where role = #{getRole}")
    public int getRoleId(String getRole);

    @Insert("insert into tb_users_role (user_id, role_id) values(#{userId}, #{roleId})")
    public Boolean insertUserRole(String userId, int roleId);

    @Select("select *from tb_users where username=#{username} and password=#{password}")
    public User checkUserLogin(String username, String password);

    @Select("select is_acc_close from tb_users where username=#{username}")
    Boolean checkAccIsClose(String username);

}
